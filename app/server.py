import os
from flask import abort, Flask, redirect, render_template, request, send_file
from lib import file, tabella, template

app = Flask(__name__, template_folder = 'pagine')

# Home page
@app.route('/')
def home_page():
	return render_template('home_page.html')

# Pagina per l'elaborazione
@app.route('/elaborazione', methods = ['GET', 'POST'])
def elaborazione():
	# Se l'utente richiede la pagina senza impostare le variabili POST, fallo
	# tornare alla home page
	if request.method != 'POST':
		return redirect('/')

	# Ottieni i file dal POST e sistemali nella cartella degli upload
	try:
		file.ottieni_file(request.files['template'], request.files['codici'])
	except ValueError as errore:
		return render_template('home_page.html', errore = str(errore))

	# Preleva i codici dal file CSV
	codici = tabella.estrai_codici(file.CARTELLA_UPLOAD + 'codici.csv')

	# Genera il file PDF
	html = template.genera_html(codici)
	pdf = template.genera_pdf(html)
	template.salva_pdf(pdf, file.CARTELLA_UPLOAD  + 'codici.pdf')

	# Redirigi l'utente alla pagina del download
	return redirect('download?tipo=pdf')

# Pagina per i download
@app.route('/download', methods = ['GET'])
def download():
	# Ottieni il tipo del file da mostrare
	tipo = request.args.get('tipo')

	# Se la variabile 'tipo' è uguale a 'logo' o a 'logo_mynet', ritorna il logo
	# dell'azienda o il logo della MyNet
	if tipo in ['logo', 'logo_mynet']:
		# Preferisci le immagini PNG a quelle JPEG
		path = file.CARTELLA_TEMPLATE + tipo
		estensioni = ['.png', '.jpg', '.jpeg']

		# Se esiste l'immagine con una delle estensioni, ritornala
		for estensione in estensioni:
			if os.path.exists(path + estensione):
				return send_file(path + estensione)

	# Se la variabile 'tipo' è uguale a 'pdf', ritorna il file PDF
	elif tipo == 'pdf':
		if os.path.exists(file.CARTELLA_UPLOAD + 'codici.pdf'):
			return send_file(file.CARTELLA_UPLOAD + 'codici.pdf')

	return abort(404)

# # Pagina per il test
# #
# # Questa pagina serve a generare dei codici di prova con il file 'index.html'
# # presente nella cartella 'working_dir/template'. Utile se si vuole creare un
# # nuovo template HTML
# @app.route('/test')
# def test():
# 	return template.genera_html(['codiceDiProva1', 'codiceDiProva2', 'codiceDiProva3'])

if __name__ == '__main__':
	app.run()
