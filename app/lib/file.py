import os
import shutil
from tarfile import open as open_tar
from zipfile import ZipFile as open_zip
from werkzeug.utils import secure_filename

CARTELLA_UPLOAD = 'working_dir/'
CARTELLA_TEMPLATE = CARTELLA_UPLOAD + 'template/'

def ottieni_file(template, codici):
	# Se la cartella per gli upload esiste già, eliminala
	if os.path.exists(CARTELLA_UPLOAD):
		shutil.rmtree(CARTELLA_UPLOAD)

	# Se l'utente non ha inserito il template o i codici, genera un eccezione
	if template.filename == '' or codici.filename == '':
		raise ValueError('Inserisci entrambi i file!')

	# Se il template non è né in formato '.tar.gz' né '.zip', genera un eccezione
	if not (template.filename.endswith('.tar.gz') or template.filename.endswith('.zip')):
		raise ValueError('Il template deve essere in formato ".tar.gz" o ".zip"!')

	# Se i codici non sono in formato '.csv', genera un eccezione
	if not codici.filename.endswith('.csv'):
		raise ValueError('I codici devono essere in formato ".csv"!')

	# Crea la cartella per gli upload
	os.makedirs(CARTELLA_UPLOAD)

	# Ottieni i percorsi per il salvataggio dei file
	percorso_template = CARTELLA_UPLOAD + secure_filename(template.filename)
	percorso_codici = CARTELLA_UPLOAD + 'codici.csv'

	# Salva i file
	template.save(percorso_template)
	codici.save(percorso_codici)

	# Estrai il template in caso fosse un archivio '.tar.gz'
	if percorso_template.endswith('.tar.gz'):
		with open_tar(percorso_template) as template:
			template.extractall(CARTELLA_TEMPLATE)

	# Estrai il template in caso fosse un archivio '.zip'
	elif percorso_template.endswith('.zip'):
		with open_zip(percorso_template) as template:
			template.extractall(CARTELLA_TEMPLATE)

	# Se se il template non contiene il file 'index.html', genera un eccezione
	if not os.path.exists(CARTELLA_TEMPLATE + 'index.html'):
		shutil.rmtree(CARTELLA_UPLOAD)
		raise ValueError('Il template deve contenere almeno il file "index.html"!')

	# Rimuovi l'archivio compresso
	os.remove(percorso_template)
