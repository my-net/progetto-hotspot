from flask import request
from jinja2 import Template
from weasyprint import HTML
from lib.file import CARTELLA_TEMPLATE

def genera_html(codici):
	# Leggi il contenuto del file 'index.html'
	with open(CARTELLA_TEMPLATE + 'index.html', 'r', encoding = 'utf-8') as file:
		template = Template(file.read())

	# Sostituisci i placeholder con i codici e ritorna l'HTML
	return template.render(codici = codici)

def genera_pdf(html):
	# Genera un PDF dall'HTML e ritornalo
	return HTML(string = html, base_url = request.base_url).write_pdf()

def salva_pdf(pdf, nome_file):
	with open(nome_file, 'wb') as file:
		file.write(pdf)
