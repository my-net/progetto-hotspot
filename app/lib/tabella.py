import csv

def estrai_codici(nome_file):
	# Crea un array che conterrà i codici
	codici = []

	# Apri il file 'codici.csv'
	with open(nome_file, 'r', encoding = 'utf-8') as file:
		lettore = csv.reader(file)

		# Per ogni linea, inserisci nell'array 'codici' la prima colonna
		for _, linea in enumerate(lettore):
			codici.append(linea[0])

	# Ritorna i codici
	return codici
